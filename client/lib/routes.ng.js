angular.module('articlesApplication').run(function ($rootScope, $state) {
    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        if (error === 'AUTH_REQUIRED') {
            $state.go('articlesList');
        }
    });
});

angular.module('articlesApplication').config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $stateProvider
        .state('articlesList', {
            url: '/articles',
            templateUrl: 'client/articles/views/articles-list.ng.html',
            controller: 'ArticlesController'
        })
        .state('articleDetail', {
            url: '/articles/:articleId',
            templateUrl: 'client/articles/views/article-detail.ng.html',
            controller: 'ArticleDetailController',
            resolve: {
                'currentUser': function ($meteor) {
                    return $meteor.requireUser();
                }
            }
        });

    $urlRouterProvider.otherwise('/articles');
});