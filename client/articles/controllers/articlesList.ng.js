angular.module('articlesApplication').controller('ArticlesController', function ($scope, $meteor, $rootScope) {

    $scope.page = 1;
    $scope.perPage = 2;
    $scope.sort = {title: 1};
    $scope.orderProperty = '1';

    $scope.$meteorSubscribe('users');

    $scope.articles = $meteor.collection(function() {
        return Articles.find({}, {
            sort: $scope.getReactively('sort')
        });
    });

    $meteor.autorun($scope, function () {
        $meteor.subscribe('articles', {
            limit: parseInt($scope.getReactively('perPage')),
            skip: parseInt(($scope.getReactively('page') - 1) * $scope.getReactively('perPage')),
            sort: $scope.getReactively('sort')
        }, $scope.getReactively('search')).then(function () {
            $scope.articlesCount = $meteor.object(Counts, 'numbersOfArticles', false);
        });
    });

    $scope.remove = function (article) {
        $scope.articles.remove(article);
    };

    $scope.removeAll = function () {
        $scope.articles.remove();
    };

    $scope.pageChanged = function (newPage) {
        $scope.page = newPage;
    };

    $scope.$watch('orderProperty', function () {
        if ($scope.orderProperty) {
            $scope.sort = {title: parseInt($scope.orderProperty)};
        }
    });

    $scope.getUserById = function(userId) {
        return Meteor.users.findOne(userId);
    }

    $scope.creator = function (article) {
        if (!article) {
            return;
        }
        var owner = $scope.getUserById(article.owner);
        if (!owner) {
            return 'nobody';
        }

        if ($rootScope.currentUser && $rootScope.currentUser._id) {
            if (owner._id === $rootScope.currentUser._id) {
                return 'me';
            }
        }
        return owner;
    }
});
