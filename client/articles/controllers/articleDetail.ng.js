angular.module('articlesApplication').controller('ArticleDetailController', function ($scope, $stateParams, $meteor) {
    $scope.article = $meteor.object(Articles, $stateParams.articleId, false);
    $scope.users = $meteor.collection(Meteor.users, false).subscribe('users');

    var savedSubscription = null;
    $scope.$meteorSubscribe('articles').then(function(handle) {
        savedSubscription = handle;

        savedSubscription.stop();
    });

    $scope.save = function () {
        $scope.article.save().then(function (numberOfDocs) {
            console.log('save successfull ', numberOfDocs);
        }, function (error) {
            console.log('save error', error)
        });
    };

    $scope.reset = function () {
        $scope.article.reset();
    };
});