angular.module('articlesApplication').filter('uninvited', function () {
    return function (users, article) {
        if (!article) {
            return false;
        }
        return _.filter(users, function (user) {
            return !(user._id == article.owner ||
                _.contains(article.invited, user._id));
        });
    }
});
