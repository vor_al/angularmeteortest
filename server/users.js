Meteor.publish('users', function () {
    return Meteor.users.find({}, {emails: 1, profile: 1});
});