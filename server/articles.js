Meteor.publish('articles', function (options, searchString) {
    if (searchString == null) {
        searchString = '';
    }
    Counts.publish(this, 'numbersOfArticles', Articles.find({
        'title': {'$regex': '.*' + searchString || '' + '.*', '$options' : 'i'},
        $or: [
            {
                $and: [
                    {"public": true},
                    {"public": {$exists: true}}
                ]
            },
            {
                $and: [
                    {owner: this.userId},
                    {owner: {$exists: true}}
                ]
            }
        ]
    }), {noReady: true});
    return Articles.find({
        'title': {'$regex': '.*' + searchString || '' + '.*', '$options' : 'i'},
        $or: [
            {
                $and: [
                    {"public": true},
                    {"public": {$exists: true}}
                ]
            },
            {
                $and: [
                    {owner: this.userId},
                    {owner: {$exists: true}}
                ]
            }
        ]
    }, options);
});
